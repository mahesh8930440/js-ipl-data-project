function strikeRateOfBatsman(allDeliveries,allMatches,batsman){
    const seasonAndMatchIdData={};
    
    for (let index=0;index<allMatches.length;index++){
        let season=allMatches[index].season;

        if(!(seasonAndMatchIdData[season])){
            seasonAndMatchIdData[season]=[];
        }
        let matchId=allMatches[index].id;
        seasonAndMatchIdData[season].push(matchId);
    }
    let allBall=0;
    let totalBatsmanrun=0;
    let batsmanStrikeRates=0;
    const allbatsmanStrikeRates={}

    for (let keySeason in seasonAndMatchIdData){
        let matchesId= seasonAndMatchIdData[keySeason];
        allBall=0;
        totalBatsmanrun=0;
        batsmanStrikeRates=0;
        
        for(let index=0;index<allDeliveries.length;index++){
            const deliveryMatchId=allDeliveries[index].match_id;
        
            if((matchesId.includes(deliveryMatchId)) &&(allDeliveries[index].wide_runs==0) &&(allDeliveries[index].batsman==batsman)){
                allBall +=1;
                totalBatsmanrun += parseInt(allDeliveries[index].batsman_runs);
            }
        }

        if (allBall > 0) {
            batsmanStrikeRates = ((totalBatsmanrun / allBall) * 100).toFixed(2);
        }
        
        else {
            batsmanStrikeRates = 0; 
        }

        allbatsmanStrikeRates[keySeason]=batsmanStrikeRates;
    }
    return (allbatsmanStrikeRates);
    
    
} 

module.exports=strikeRateOfBatsman;