function topEconomicalBowler(allDeliveries,allMatches){
  const seasonAndMatchIdData=[];
  
  for (let index=0;index<allMatches.length;index++){
    let season=allMatches[index].season;

    if (season=="2015"){
      let matchId=allMatches[index].id;
      seasonAndMatchIdData.push(matchId);
    } 
  }
  const allBowlerData= {};

  for(let id of seasonAndMatchIdData){
    
    for (let index = 0; index < allDeliveries.length; index++) {
      
      if(allDeliveries[index].match_id == id){
        const runsConceded = parseInt(allDeliveries[index].total_runs);
        const byeRuns = parseInt(allDeliveries[index].bye_runs) ;
        const wideRuns =parseInt(allDeliveries[index].wide_runs );
        const bowler = allDeliveries[index].bowler;

        if (!allBowlerData[bowler]) {
          allBowlerData[bowler] = { runs: runsConceded, balls: 1 };
        } 
        
        else {
          allBowlerData[bowler].runs += runsConceded;
          allBowlerData[bowler].balls += 1;
        }

        if ((byeRuns!=0 )|| (wideRuns!=0)) {
          allBowlerData[bowler].balls -= 1;
          allBowlerData[bowler].runs -=byeRuns;
        }
      }
    }  
  }
  
  for (const bowler in allBowlerData) {
    const economy = ((allBowlerData[bowler].runs / allBowlerData[bowler].balls)*6).toFixed(2);
    allBowlerData[bowler].economy = economy;
  }
  const allBowlerEconomy=[];

  for(let keyBowler in allBowlerData){
      let bowler=keyBowler;
      let economy=allBowlerData[keyBowler].economy;
      allBowlerEconomy.push([bowler,economy]);
  }
  const topBowlerEconomy=allBowlerEconomy.sort((bowler1,bowler2) => parseFloat(bowler1[1]) - parseFloat(bowler2[1]));
  return (topBowlerEconomy.slice(0,10));
}

module.exports=topEconomicalBowler;